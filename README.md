# <img src="https://icons.iconarchive.com/icons/papirus-team/papirus-apps/256/youtrack-icon.png" width="4%" /> YouTrack - Custom Issue Icons
_Replace the "Priority"-only issue icons with any other field you have set up._

[[_TOC_]]

## Features

- Supports multiple accounts.
- Each account can display a different field.
- Dynamically updates the issue icon when field value is changed.

<img src="https://gitlab.com/AlexandreSP/youtrack-custom-issue-icons/-/wikis/images/compare.png" width="75%"/>

## Installation

Download the script over at [Greasy Fork](https://greasyfork.org/en/scripts/429195-youtrack-custom-issue-icons)!

## Configuration

You will find at the top of the file `YOUTRACK_PROFILES`. In this object, you will be able to list any number of YouTrack profiles you may own:

```javascript
const YOUTRACK_PROFILES = {
    "location.hostname": {
        token: "perm:***********",
        replacementField: "Field"
    }
};
```

1. **Key:** Domain accessed. Should reflect `location.hostname`.
    - Example: `myproject.myjetbrains.com`
2. **Token:** Authentication token.
    - Follow this [documentation](https://www.jetbrains.com/help/youtrack/standalone/Manage-Permanent-Token.html#obtain-permanent-token) if you do not know how to generate an authentication token.
3. **ReplacementField:** Name of the field that should be used as a replacement.
    - Example: `Type`, `State`, `Subsystem`...

You are not expected to modify anything more in the file.

## Report an Issue

If you ever encounter an issue with the user script and do not have a GitLab account, you can create a ticket by sending an email and using the following template. Otherwise, you can submit an issue through the [interface](https://gitlab.com/AlexandreSP/youtrack-custom-issue-icons/-/issues).

Email: [`contact-project+alexandresp-youtrack-custom-issue-icons-report@incoming.gitlab.com`](mailto:contact-project+alexandresp-youtrack-custom-issue-icons-report@incoming.gitlab.com)
<details>
<summary>Click here to display email issue template:</summary>

```markdown
## Summary

(Summarize the bug encountered)

## Steps to reproduce

(How one can reproduce the issue - this is very important)

## What is the current bug behavior?

(What actually happens)

## What is the expected correct behavior?

(What you should see instead)

## Relevant logs and/or screenshots

- Operating System:
- Browser (and its version):

(Paste any relevant logs - please use code blocks (```) to format console output, logs, and code.)

## Possible fixes

(If you can, link to the line of code that might be responsible for the problem)

/label ~bug ~open
/assign @AlexandreSP
```
</details>

## Suggest a Feature

Feel free to suggest new features for the user script by using the following email and template. If you have a GitLab account, you can directly create a ticket through the [interface](https://gitlab.com/AlexandreSP/youtrack-custom-issue-icons/-/issues).

Email: [`contact-project+alexandresp-youtrack-custom-issue-icons-report@incoming.gitlab.com`](mailto:contact-project+alexandresp-youtrack-custom-issue-icons-report@incoming.gitlab.com)

<details>
<summary>Click here to display email feature template:</summary>

```markdown
## Summary

(Summarize the feature)

## Behavior

(Explain how this feature should behave, including any edge cases if relevant)

## Use Case

(Explain why this feature would add value to the user script)

/label ~suggestion ~open
/assign @AlexandreSP
```
</details>
